# URLs

Las URLs de las configuraciones:

- http://localhost:8888/springcloudclient/dev
- http://localhost:8888/springcloudclient/test
- http://localhost:8888/springcloudclient/prod

Las URLs dependen de los nombres de los ficheros que hay en el git https://github.com/jordiascension/SpringCloudConfig-Server. En el servidor los ficheros son:

- springcloudclient-dev.yml
- springcloudclient-test.yml
- springcloudclient-prod.yml

En la búsqueda de las ubicaciones de los archivos de configuración se siguen los siguientes patrones, en el caso del ejemplo he usado la segunda opción, el primero que se encuentre es el que se usa:

- /{application}/{profile}[/{label}]
- /{application}-{profile}.yml
- /{label}/{application}-{profile}.yml
- /{application}-{profile}.properties       <-- este es el activo en este caso
- /{label}/{application}-{profile}.properties
